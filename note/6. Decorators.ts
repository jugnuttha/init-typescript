function logMethods(constructor: Function) {
    console.log(Object.keys(constructor.prototype))
}

@logMethods
class God {
    createTheWorld() {}
    nap() {}
}

// ['createTheWorld','nap']



function logMethods2(startWith: string) {
    return (constructor: Function) => {
        console.log(
            Object.keys(constructor.prototype)
            .filter(method => method.startsWith(startWith))
            )
    }
    
}

@logMethods2('create')
class God2 {
    createTheWorld() {}
    createHuman() {}
    nap() {}
}


// properties decorator
function validate(regex: RegExp ) {
    return (target: any,  key: string ) => {
        let _value = this[key]
        Object.defineProperty(target,key, {
            set(value: string) {
                if (regex.test(value)) _value = value
                else throw new Error('Invalid value') 
            },
            get() {
                return _value;
            }
        })
    }
}

class Person {
    @validate(/[a-zA-Z]+/)
    firstName: string

    @validate(/[a-zA-Z]+/)
    lastName: string
}


// Decorator Composition
function f() {
    console.log('@f')
}

function g() {
    console.log('@g')
}

@f
@g
class Fool {}

// f(g(Foo))

