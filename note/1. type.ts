let isDone: boolean = false
let decimal: number = 6
let color:string = 'blue'

// Array
let list: number[] = [1,2,3]
let list2: Array<number> = [1,2,3]

// tuple type
let x: [string,number] = ["hello",10]

// Any
let notSure: any =4

// void
function warnUser(): void {
    alert('x')
}

// null & undefine
let u: undefined = undefined
let n: null = null

// never ไม่คืนค่ากลับอย่างแท้จริง
function error(message:string): never {
    throw new Error(message)
}


// Literal type
let foo:'Bar'

let trafficLight: 'Red'|'Green'|'Yellow'
trafficLight = 'Red'

// Enum
enum Color {Red,Green,Blue}
let c: Color = Color.Green

enum Colors {Red=1,Green=2,Blu3}
let c2: number = Color.Green


// Type Assertion
let someValue: any = "this is string"
let strLenght: number = (<string> someValue).length;

