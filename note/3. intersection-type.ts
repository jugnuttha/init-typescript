interface Shape { getArea(): number }
interface Logger { log(): void }

class Circle implements Shape, Logger {

    getAreas(): number {
        return 1;
    }
    
    log(): void {
        
    }
}

const circle: Circle = new Circle()

// Union type
 const circle2: Shape | Logger = new Circle();

function slugify( text : string | string[] ):string {
    let result: string;
    if ( typeof text === 'string') {
        result = text
    }else{
        result = text.join(' ');
    }
    return result
}


// Index Signature
interface Point{
    x: number
    y: number
    z: number
}
interface Point2 {
    [key:string]: number
}

const p1 : Point2 ={
    x:1,
    y:2,
    z:3
}

