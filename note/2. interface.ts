interface Rectangle{
    width:number
    height:number
    zindex?:number      // optional
}

function getArea(rec: Rectangle): number {
    return rec.width * rec.height
}

const rec1 : Rectangle = {
    width:100,
    height:100
}