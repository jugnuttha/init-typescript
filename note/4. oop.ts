class Car {
    lng: number

    constructor(public speed = 0, readonly lat: number) {
        this.lng = undefined
    }

}

const car = new Car(10,1)

// Accessors
class Human {
    private _firstName : string
    private _lastName : string

    constructor(fullName: string) {
        this.fullName = fullName
    }

    set fullName(fullName: string) {
        [this._firstName,this._lastName] = fullName.split(' ')
    }

    get firstname() : string { return this._firstName }


}

// Inheritance
class Woman extends Human {
    pregnant(){

    }
}

const somsree = new Woman('s a')
somsree.fullName


// public, private, protected, static

// abstract
abstract class Shape {
    constructor(protected width: number, protected height: number ) {}

    public abstract getAreas():number;
}

// Generic
class Stack<T> {
    private data:T[] = [];
     push = (item :T): number => this.data.push(item)
     pop = () :T => this.data.pop()
}