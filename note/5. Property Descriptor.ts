const circle4: any = {}

Object.defineProperty(circle4, 'PI', {value:3.14})

// data descriptor
Object.defineProperty(circle4, 'PI', {value:3.14, writable:false, configurable:false, enumerable:false})

// accessor descriptor
const circle3: any = {_radius:10}

Object.defineProperty(circle2,'radius',{
    get(): number {
        return this._radius;
    },
    set(radius:number){
        this._radius = radius
    }
})

circle3.radius = 9;


